package io.lemniscate.marsrover.geography;

public interface Direction {
    Direction left();
    Direction right();
    Position forward(Position position);
    Position backward(Position position);
}
