package io.lemniscate.marsrover.geography;

public class North implements Direction {
    @Override
    public Direction left() {
        return new West();
    }

    @Override
    public Direction right() {
        return new East();
    }

    @Override
    public Position forward(Position position) {
        return position.north();
    }

    @Override
    public Position backward(Position position) {
        return position.south();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof North;
    }
}
