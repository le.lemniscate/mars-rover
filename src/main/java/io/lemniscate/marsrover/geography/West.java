package io.lemniscate.marsrover.geography;

public class West implements Direction {
    @Override
    public Direction left() {
        return new South();
    }

    @Override
    public Direction right() {
        return new North();
    }

    @Override
    public Position forward(Position position) {
        return position.west();
    }

    @Override
    public Position backward(Position position) {
        return position.east();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof West;
    }
}
