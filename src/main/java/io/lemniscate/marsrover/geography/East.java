package io.lemniscate.marsrover.geography;

public class East implements Direction {
    @Override
    public Direction left() {
        return new North();
    }

    @Override
    public Direction right() {
        return new South();
    }

    @Override
    public Position forward(Position position) {
        return position.east();
    }

    @Override
    public Position backward(Position position) {
        return position.west();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof East;
    }
}
