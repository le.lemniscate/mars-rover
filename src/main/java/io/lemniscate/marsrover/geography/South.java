package io.lemniscate.marsrover.geography;

public class South implements Direction {
    @Override
    public Direction left() {
        return new East();
    }

    @Override
    public Direction right() {
        return new West();
    }

    @Override
    public Position forward(Position position) {
        return position.south();
    }

    @Override
    public Position backward(Position position) {
        return position.north();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof South;
    }
}
