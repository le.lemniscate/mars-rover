package io.lemniscate.marsrover.geography;

import java.util.Objects;

public class Position {

    private int x;
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x && y == position.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public Position north() {
        return new Position(x, y + 1);
    }

    public Position south() {
        return new Position(x, y - 1);
    }

    public Position east() {
        return new Position(x + 1, y);
    }

    public Position west() {
        return new Position(x - 1, y);
    }
}
