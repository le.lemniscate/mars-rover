package io.lemniscate.marsrover;

import io.lemniscate.marsrover.geography.Direction;
import io.lemniscate.marsrover.geography.Position;

public class MarsRover {

    private Direction direction;
    private Position position;

    public MarsRover(Position position, Direction direction) {
        this.direction = direction;
        this.position = position;
    }

    void command(String command) {
        switch (command) {
            case "f":
                this.position = direction.forward(position);
                break;
            case "b":
                this.position = direction.backward(position);
                break;
            case "l":
                this.direction = direction.left();
                break;
            case "r":
                this.direction = direction.right();
                break;
        }
    }

    boolean faces(Direction direction) {
        return this.direction.equals(direction);
    }

    boolean isAtPosition(Position position) {
        return this.position.equals(position);
    }
}
