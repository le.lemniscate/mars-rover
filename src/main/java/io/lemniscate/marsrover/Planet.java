package io.lemniscate.marsrover;

import io.lemniscate.marsrover.geography.Position;

public class Planet {
    private int width;
    private int height;

    public Planet(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public Position generateRandomPosition() {
        return new Position(
                (int) Math.random() * width,
                (int) Math.random() * height
        );
    }

}
