package io.lemniscate.marsrover.commands;

public enum Command {
    FORWARD,
    BACKWARD,
    TURN_LEFT,
    TURN_RIGHT
}
