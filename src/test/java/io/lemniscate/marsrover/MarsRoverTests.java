package io.lemniscate.marsrover;

import io.lemniscate.marsrover.geography.*;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

public class MarsRoverTests {
    @Test
    void when_created_rover_faces_given_direction_at_given_position() {
        // Given
        Planet planet = randomPlanet();
        Position position = planet.generateRandomPosition();
        Direction direction = randomDirection();

        // When
        MarsRover rover = new MarsRover(position, direction);

        // Then
        assertThat(rover.faces(direction)).isTrue();
        assertThat(rover.isAtPosition(position)).isTrue();
    }

    @Test
    void given_a_rover_facing_north_at_0_0_when_it_moves_forward_its_position_is_0_1_still_facing_north() {
        // Given
        Position position = new Position(0, 0);
        Direction direction = new North();
        MarsRover rover = new MarsRover(position, direction);

        // When
        rover.command("f");

        // Then
        assertThat(rover.faces(new North())).isTrue();
        assertThat(rover.isAtPosition(new Position(0, 1))).isTrue();
    }

    @Test
    void given_a_rover_facing_north_at_0_1_when_it_moves_backward_its_position_is_0_0_still_facing_north() {
        // Given
        Position position = new Position(0, 1);
        Direction direction = new North();
        MarsRover rover = new MarsRover(position, direction);

        // When
        rover.command("b");

        // Then
        assertThat(rover.faces(new North())).isTrue();
        assertThat(rover.isAtPosition(new Position(0, 0))).isTrue();
    }

    @Test
    void given_a_rover_facing_north_when_it_turns_left_it_faces_west_and_do_not_move() {
        // Given
        Position position = new Position(0, 0);
        Direction direction = new North();
        MarsRover rover = new MarsRover(position, direction);

        // When
        rover.command("l");

        // Then
        assertThat(rover.faces(new West())).isTrue();
        assertThat(rover.isAtPosition(new Position(0, 0))).isTrue();
    }

    @Test
    void given_a_rover_facing_north_when_it_turns_right_it_faces_east_and_do_not_move() {
        // Given
        Position position = new Position(0, 0);
        Direction direction = new North();
        MarsRover rover = new MarsRover(position, direction);

        // When
        rover.command("r");

        // Then
        assertThat(rover.faces(new East())).isTrue();
        assertThat(rover.isAtPosition(new Position(0, 0))).isTrue();
    }

    @Test
    void given_a_rover_facing_north_check_a_360_right() {
        // Given
        Position position = new Position(0, 0);
        Direction direction = new North();
        MarsRover rover = new MarsRover(position, direction);

        rover.command("r");
        assertDirectionAndPosition(rover, new East(), 0, 0);

        rover.command("r");
        assertDirectionAndPosition(rover, new South(), 0, 0);

        rover.command("r");
        assertDirectionAndPosition(rover, new West(), 0, 0);

        rover.command("r");
        assertThat(rover.faces(new North())).isTrue();
        assertDirectionAndPosition(rover, new North(), 0, 0);
    }

    @Test
    void given_a_rover_facing_north_check_a_360_left() {
        // Given
        Position position = new Position(0, 0);
        Direction direction = new North();
        MarsRover rover = new MarsRover(position, direction);

        rover.command("l");
        assertDirectionAndPosition(rover, new West(), 0, 0);

        rover.command("l");
        assertDirectionAndPosition(rover, new South(), 0, 0);

        rover.command("l");
        assertDirectionAndPosition(rover, new East(), 0, 0);

        rover.command("l");
        assertThat(rover.faces(new North())).isTrue();
        assertDirectionAndPosition(rover, new North(), 0, 0);
    }

    private void assertDirectionAndPosition(MarsRover rover, Direction direction, int x, int y) {
        assertThat(rover.faces(direction)).isTrue();
        assertThat(rover.isAtPosition(new Position(x, y))).isTrue();
    }

    private Direction randomDirection() {
        return new Direction[]{new North(), new West(), new East(), new South()}[(int) Math.random() * 4];
    }

    private Planet randomPlanet() {
        return new Planet((int) Math.random() * 20, (int) Math.random() * 20);
    }
}
